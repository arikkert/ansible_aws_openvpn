USER=ubuntu
INVENTORY=provision_ec2/inventory
IDENTITY="provision_ec2/$(USER).key.pem"
#CMD=ip a
HOST=$$(cat $(INVENTORY) | tail -1 | awk '{ print $$1 }')

.PHONY: provision_ec2 provision_openvpn

all: deploy

ansibleping: provision_ec2
	ansible -m ping -i $(INVENTORY) --private-key $(IDENTITY) thehost

ssh: provision_ec2
	ssh -i $(IDENTITY) $(USER)@$(HOST) $(CMD)

provision_ec2:
	cd $@;  \
	make

provision_openvpn:
	cd $@;  \
	make

deploy: provision_ec2 provision_openvpn
