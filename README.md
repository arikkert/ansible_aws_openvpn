# ansible_aws_openvpn

Create an openvpn server on AWS EC2 automatically. Only credentials are needed. Keys are automatically generated unique to each deployment and downloaded to your ansible depoly host

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

you need AWS credentials, ansible configured with boto3 and make.
Just run make on the ansible deploy host 

## Usage

run make to deploy the ec2 instance and provision the instance with openvpn.
So there are two steps
- provision_ec2 : for creating the instance (or destroying if specified)
- provision_openvpn : for openvpn configuration
After the instance is known, the inventory will be created for use by provision_openvpn.
The inventory can also be used for debugging.
When both steps are fininished a client.ovpn file is created and downloaded to the ansible host. This can be imported to an openvpn client : https://openvpn.net/community-downloads/

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
The code shows some links to webpages for inspiration

## License
For open source projects, say how it is licensed.

## Project status
just created it

